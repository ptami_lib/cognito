package cognito

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

func CreateUser(userPoolId string, username string, temporaryPassword string, userAttributesMap map[string]interface{}) (adminCreateUserOutput *cognitoidentityprovider.AdminCreateUserOutput, err error) {
	cognitoClient := getClient()

	newUserData := &cognitoidentityprovider.AdminCreateUserInput{
		DesiredDeliveryMediums: []*string{
			aws.String("EMAIL"),
		},
		UserPoolId:        aws.String(userPoolId),
		Username:          aws.String(username),
		TemporaryPassword: &temporaryPassword,
	}

	userAttributes := make([]*cognitoidentityprovider.AttributeType, len(userAttributesMap))
	index := 0
	for key, value := range userAttributesMap {
		valueString := value.(string)
		userAttributes[index] = &cognitoidentityprovider.AttributeType{
			Name:  aws.String(key),
			Value: aws.String(valueString),
		}
		index++
	}

	newUserData.SetUserAttributes(userAttributes)

	adminCreateUserOutput, err = cognitoClient.AdminCreateUser(newUserData)

	return
}

func ResetPassword(userPoolId string, username string, password string, permanent bool) (adminSetUserPasswordOutput *cognitoidentityprovider.AdminSetUserPasswordOutput, err error) {
	cognitoClient := getClient()

	passwordData := &cognitoidentityprovider.AdminSetUserPasswordInput{
		Password:   aws.String(password),
		Permanent:  aws.Bool(permanent),
		UserPoolId: aws.String(userPoolId),
		Username:   aws.String(username),
	}

	adminSetUserPasswordOutput, err = cognitoClient.AdminSetUserPassword(passwordData)

	return
}

func getClient() (client *cognitoidentityprovider.CognitoIdentityProvider) {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	return cognitoidentityprovider.New(sess)
}
